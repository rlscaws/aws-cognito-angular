import {Component} from "@angular/core";
import {UserLoginService} from "../../service/user-login.service";
import {Callback, CognitoUtil, LoggedInCallback} from "../../service/cognito.service";
import {UserParametersService} from "../../service/user-parameters.service";
import {Router} from "@angular/router";
import {HttpClient, HttpHeaders} from "@angular/common/http";



@Component({
    selector: 'awscognito-angular2-app',
    templateUrl: './search.html'
})
export class SearchComponent implements LoggedInCallback {
  public parameters: Array<Parameters> = [];
  public cognitoId: String;

  constructor(public router: Router, public userService: UserLoginService, public userParams: UserParametersService, public cognitoUtil: CognitoUtil, private http: HttpClient) {
      this.userService.isAuthenticated(this);
      console.log("In SearchComponent");
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
      if (!isLoggedIn) {
          this.router.navigate(['/login']);
      } else {
        this.userParams.getParameters(new GetParametersCallback(this, this.cognitoUtil));
      }
  }

  private payload:string[] = [];
  private columnNames:string[] = [];
  private hasSubmitted:boolean = false;
  private currentPage:number;
  private hasDownloaded:boolean = false;
  private downloadReady:boolean = false;
  private downloadUrl: string = "";

  private onQuery(value:string, user:string) {
    this.hasSubmitted = true;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    let queryStatement = value.trim();
    let nickname = user.toLowerCase();
    let body = {
      userName: nickname,
      statement: queryStatement
    };
    //CHECK IF PID EXISTS
    this.http.post("https://mmy118tj8d.execute-api.us-east-2.amazonaws.com/dev",
      body,
      httpOptions)
      .subscribe(
      res => {
        if(res !=null) {
          console.log("PID FOUND, CANCELLING EXISTING QUERY");
          this.cancelPid(res);
        }
        // DROP TABLE
        this.http.post('https://cc64ilsfe3.execute-api.us-east-2.amazonaws.com/dev',
          body,
          httpOptions)
          .subscribe(
            res => {
              this.pollTable(value, nickname);
              console.log('TABLE DROPPED SUCCESSFULLY', res);
              //CREATE TABLE
              this.http.post('https://6dremn7x45.execute-api.us-east-2.amazonaws.com/dev',
                body,
                httpOptions)
                .subscribe(
                  resp => {
                    console.log('TABLE CREATED SUCCESSFULLY', resp);
                  }, err => {
                    console.log("Error");
                  }
              );//CLOSE CREATE TABLE
            }, err => {
              console.log("Error");
            }
        );// CLOSE DROP TABLE
      }, err => {
        console.log("Error");
      }
    );//CLOSE PID CHECK
  }

  private pollTable(value:string, nickname:string) {
    let queryStatement = value.trim();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    let body = {
      statement: queryStatement,
      userName: nickname
    };
    let interval = setInterval(() => {
      this.http.post('https://lrzjypz255.execute-api.us-east-2.amazonaws.com/dev',
        body,
        httpOptions)
        .subscribe(
          response => {
            let rData = JSON.stringify(response);
            let rDataP = JSON.parse(rData);
            let status = rDataP.statusCode;
            if(status == 200) {
              clearInterval(interval);
              this.simpleQuery(value, nickname);
            }
            console.log('DATA DELIVERED', status);
          },
          err => {
            console.log("Error");
          }
      );
    }, 2000);
  }

  private simpleQuery(value:string, nickname:string) {
    console.log("text box value: " + value);
    console.log('nickname ' + nickname);
    let queryStatement = value.trim();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    let body = {
      statement: queryStatement,
      userName: nickname
    };
    this.http.post('https://brbq3zf4f7.execute-api.us-east-2.amazonaws.com/dev',
      body,
      httpOptions)
      .subscribe(
        res => {
          let strData = JSON.stringify(res);
          let obj = JSON.parse(strData);
          let count = 0;
          //Get column names
          for(let key in obj) {
            count++;
            let val = obj[key];
            if(count == 1) {
              for(let x in val) {
                this.columnNames.push(x);
              }
            }
            //All values
            this.payload.push(obj[key]);
          }
          //Re-enable Query button
          this.hasSubmitted = false;
          this.currentPage = 1;
        },
        err => {
          console.log("Error");
        }
      );
  }

  private paginate(page:number, user:string) {
    let nickname = user.toLowerCase();
    let pageNumber = (page - 1) * 20;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    let body = {
      offset: pageNumber,
      userName: nickname
    };
    this.http.post('https://3bguvbxld8.execute-api.us-east-2.amazonaws.com/dev',
      body,
      httpOptions)
      .subscribe(
        res => {
          let strData = JSON.stringify(res);
          let obj = JSON.parse(strData);

          this.payload.splice(0);
          for(let key in obj) {
            this.payload.push(obj[key]);
          }
          this.currentPage = page;
        },
        err => {
          console.log("Error");
        }
      );
  }

  private cancelPid(pidResponse:any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    let body = pidResponse;
    this.http.post('https://7t0d09m2t0.execute-api.us-east-2.amazonaws.com/dev',
      body,
      httpOptions)
      .subscribe(
        res => {
          console.log("PID CANCELLED");
          console.log(res);
        }, err => {
          console.log("Error");
        }
      );
  }

  private onDownloadRequest(user:string) {
    this.hasDownloaded = true;
    let nickname = user.toLowerCase();
    // let bucket = "angular-rm-test/unload/usr_" + nickname;
    let file = "usr_" + nickname + ".csv000.gz";
    let userId = "usr_" + nickname;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    let body = {
        userName: nickname
    };
    this.http.post('https://pnngmnlc14.execute-api.us-east-2.amazonaws.com/dev',
      body,
      httpOptions)
      .subscribe(
        res => {
          console.log("DATA SENT TO S3 BUCKET");
          console.log(res);
          this.hasDownloaded = false;
          this.getS3DownloadLink(file, userId);
        }, err => {
          console.log("Error");
        }
      );
  }

  private getS3DownloadLink(file:string, user:string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    let body = {
        userName: user
    };
    //GET S3 USER BUCKET PATH
    this.http.post('https://iebcdzss6l.execute-api.us-east-2.amazonaws.com/dev',
      body,
      httpOptions)
      .subscribe(
        res => {
          let str = JSON.stringify(res);
          let parsed = JSON.parse(str);
          let bucketName = parsed.bucket;
          console.log('BUCKET: ', bucketName);
          //GET DOWNLOAD URL
          let body = {
              sourceBucket: bucketName,
              sourceFile: file
          };
          this.http.post('https://ad9orq1kx0.execute-api.us-east-2.amazonaws.com/dev',
            body,
            httpOptions)
            .subscribe(
              res => {
                console.log("GETTING S3 DOWNLOAD LINK");
                this.downloadReady = true;
                this.downloadUrl = res.toString();
                console.log(res);
              }, err => {
                console.log("Error");
              }
            );

        }, err => {
          console.log("Error");
        }
      );
  }
}

export class Parameters {
    name: string;
    value: string;
}

export class GetParametersCallback implements Callback {

    constructor(public me: SearchComponent, public cognitoUtil: CognitoUtil) {

    }

    callback() {

    }

    callbackWithParam(result: any) {
        for (let i = 0; i < result.length; i++) {
            let parameter = new Parameters();
            parameter.name = result[i].getName();
            parameter.value = result[i].getValue();
            if(parameter.name == 'nickname') {
                console.log('nickname: ' + parameter.value);
                this.me.parameters.push(parameter);
            }
        }
    }
}
