import {Component} from "@angular/core";
import {UserLoginService} from "../../service/user-login.service";
import {Callback, CognitoUtil, LoggedInCallback} from "../../service/cognito.service";
import {UserParametersService} from "../../service/user-parameters.service";
import {Router} from "@angular/router";
import {HttpClient, HttpHeaders} from "@angular/common/http";



@Component({
    selector: 'awscognito-angular2-app',
    templateUrl: './redirect.html'
})
export class RedirectComponent implements LoggedInCallback {
  public parameters: Array<Parameters> = [];
  public cognitoId: String;

  constructor(public router: Router, public userService: UserLoginService, public userParams: UserParametersService, public cognitoUtil: CognitoUtil, private http: HttpClient) {
      this.userService.isAuthenticated(this);
      console.log("In RedirectComponent");
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
      if (!isLoggedIn) {
          this.router.navigate(['/login']);
      } else {
        this.userParams.getParameters(new GetParametersCallback(this, this.cognitoUtil, this.http));
      }
  }

}

export class Parameters {
    name: string;
    value: string;
}

export class GetParametersCallback implements Callback {

    constructor(public me: RedirectComponent, public cognitoUtil: CognitoUtil, private http: HttpClient) {

    }

    callback() {

    }

    callbackWithParam(result: any) {
        for (let i = 0; i < result.length; i++) {
            let parameter = new Parameters();
            parameter.name = result[i].getName();
            parameter.value = result[i].getValue();
            if(parameter.name == 'email') {
                console.log('EMAIL: ' + parameter.value);
                this.userLookup(parameter.value);
            }
        }
    }

    private userLookup(email:string) {

      let queryStatement = "SELECT * from merck_dev.merck_user_list where email_address='" + email + "'";

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };
      let body = {
        statement: queryStatement
      };
      this.http.post('https://xbja8ipdc9.execute-api.us-east-2.amazonaws.com/dev',
        body,
        httpOptions)
        .subscribe(
          res => {
            let response = {
              isid: res[0].isid,
              email_address: res[0].email_address,
              userid: res[0].userid.toString(),
              user_name: res[0].nickname 
            }
            this.writeToDynamo(response);
          },
          err => {
            console.log("Error");
          }
        );
    }

    private writeToDynamo(userInfo:any) {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };
      let body = userInfo;
      this.http.post('https://oldif37snl.execute-api.us-east-2.amazonaws.com/dev',
        body,
        httpOptions)
        .subscribe(
          res => {
            for(let x in res) {
              let user_id = res[x];
              window.location.href="http://localhost:3000/search/?lookup=" + user_id;
            }
          },
          err => {
            console.log("Error");
          }
        );
    }
}
