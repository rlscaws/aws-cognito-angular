// index.js
console.log("LOADING INDEX.JS...");

var x = 0;  //initlal text box count
var max_fields = 10; //maximum input boxes allowed
var wrapper = $(".input_fields_wrap"); //Fields wrapper
var domID = '';
var isDownload = false; // has download been pressed
var jobReferenceId = 0; // stores BigQuery api job id for retries and accessing temp tables for summary and download
// var sortFlag = false; // is this query the result of a user hitting a column header to initiate a sort
// const $ = require('jquery');
// require('jquery-sticky-table-header');
// $('.table-container').stickyTableHeader();
//
//end globals definition

$("#fieldSelect").show();
$('.categoriesMainDiv').show();
$('.datamarts-viewsMainDiv').show();
$('.datatypesMainDiv').show();
$('.fields-resultsMainDiv').show();
$('.hr1').show();
showCategoriesLoadingSpinner();

function textInput2(x) {
    $('#searchPhraseCmpdID' + x).show();
    $('#txtinput_initiated').html('Waiting for submission...');
}

// This function builds the query to run in BigQuery after capturing compound_id in the #fltrtext textarea
function runSrchQuery() {

    resetQuery();
    resetPagination();
    document.getElementById('disablingDiv').style.display='block';
    $("#cancel_button").show();
    $("#cancel_button").prop('disabled', false);
    // getCols();
    c = wrapper.find(':selected').map(function(){ // ["city1","city2","choose io","foo"]
        return $(this).text();
    });
    pref = jQuery.extend(true,[],c);
    console.log('PREF =========> ', pref);
    $('#searchPhraseCmpdID').show();

    console.log('isEditQuery =====> ', isEditQuery);
    if (isEditQuery) {
        q = $('#queryTxtArea').val();
        var lq = q.toLowerCase();
        if (lq.indexOf("delete") > -1){
            q = "";
            lq = "";
            alert("Edit queries should not contain keyword delete!");
        }
        else{
            getResultsTable(q);
        }
    }
    else {
        console.log("selected_datamartView ",selected_datamartView);
        if (selected_category === "Correlate Data"){
            bqView = selected_datamartView
        }
        else{
            bqView = selected_datamartView.toString();
        }
        console.log("bqView ", bqView)
        if (selected_category === 'Correlate Data' && selected_datamartView.length > 1){// if correlate data call the custom wrapper for writeQuery
            q = writeUnifiedQuery(bqView);
        }
        else{
            if (selected_category === 'Correlate Data'){
                alert("WARNING: The entered search criteria only requires one view, redirecting to single view search...");
                selected_category = bqView;
            }
            q = writeQuery(bqView,false);
        }
        $('#queryTxtArea').val(q); // set the query in query builder textarea
        getResultsTable(q);
    }

}

// function called whe the minus(-) button is clicked
function removeFilterRow(i) {
    console.log("filterRows: ", filterRows);
    console.log("x: ",x);
    console.log("i: ",i);
    var ni = i + 1;
    if(i === x ){ // was the minus button clicked on the last row
        console.log("last minus!");
        filterRows.pop(); //remove last element of saved filter objects
        savedFilterInputs.pop();
        //remove this div
        $("#input_fields_wrap"+x).remove();
        x--;
    }
    else if (i < x){ // user clicked - on a row in the middle
        console.log("middle minus!")

        // loop over the filter rows in savedFilterInputs after the index where - was pressed and decrement the filterRow
        for (var j = i-1; j < savedFilterInputs.length-1; j++){
            savedFilterInputs[j] = savedFilterInputs[j+1];// copy filters up once row
            savedFilterInputs[j].filterRow--; // decrement filterRow proprerty
            // console.log(savedFilterInputs);
        }

        filterRows.pop(); // remove that filter row
        savedFilterInputs.pop(); // remove last item
        $("#input_fields_wrap" + x).remove(); // remove the last div and decrement x
        x--; //text box decrement

    }
    else{
        console.log("i > x: ", i, x)
    }
    // saveAndRetainFilterInputs();
    reApplyFilterInputs();
    saveFilterInputs();
    console.log("after: ", savedFilterInputs);
}

function addFilterRow(i) {
    console.log("filterRows: ", filterRows);
    console.log("x: ",x);
    console.log("i: ",i);
    var ni = i + 1;
    if(i === 0 || (i === x && x < max_fields) ){ // was the plus button clicked on the last row
        x++; //text box increment
        filterRows.push(x);
        $('#searchPhraseCmpdID').show();
        $(wrapper).append('<div id="input_fields_wrap' + x + '"> \
                        <div id="operator' + x + '"><select id="operatorSelect' + x + '" class="form-control" onchange="saveFilterInputs()"></select></div> \
                        <div id="tableCols' + x + '"><select id="select' + x + '" class="form-control" onchange="setDomIDIndex(this.id)"></select></div> \
                        <div id="symbol' + x + '"><select id="symbolSelect' + x + '" onchange ="showDatePicker(this.id,' + x +')" class="symbol_ddown form-control"></select></div> \
                        <div id="entry' + x + '" style="width: 300px"><textarea rows="2" id="searchPhraseCmpdID' + x +'" class="form-control" style="display: none; width: 80%" onchange="saveFilterInputs()"></textarea></div> \
                        </div>');

        $('#searchPhraseCmpdID' + x).append(textInput2(x));
        loadSelectedfields(x);
        getSymbol(x); // this is the default symbols every time a new filter row is added

        if (x === 1){
            $('#entry' + x).append('<img src="images/plusAddRow.png" class="add_field_button" width: 10%; onclick="addFilterRow(' + x + ');"/>');

        }
        else {
            $('#operatorSelect' + x).append(getOperator(x)); // we just add the operator not on the first row
            $('#entry' + x).append('<img src="images/plusAddRow.png" class="add_field_button" width: 5%; onclick="addFilterRow(' + x + ');"/> &nbsp;');
            $('#entry' + x).append('<img src="images/minusRemoveRow.png" class="remove_field" width: 5%; onclick="removeFilterRow(' + x + ');"/>');
            setDomIDIndex('select' + x);
        }
        saveFilterInputs();
        console.log("last plus!")
    }
    else if (x < max_fields && i < x){ // user clicked + on a row in the middle
        console.log("middle plus!")
        x++; //text box increment
        filterRows.push(x);
        $('#searchPhraseCmpdID').show();
        $(wrapper).append('<div id="input_fields_wrap' + x + '"> \
                        <div id="operator' + x + '"><select id="operatorSelect' + x + '" class="form-control" onchange="saveFilterInputs()"></select></div> \
                        <div id="tableCols' + x + '"><select id="select' + x + '" class="form-control" onchange="setDomIDIndex(this.id)"></select></div> \
                        <div id="symbol' + x + '"><select id="symbolSelect' + x + '" onchange ="showDatePicker(this.id,' + x +')" class="symbol_ddown form-control"></select></div> \
                        <div id="entry' + x + '" style="width: 300px"><textarea rows="2" id="searchPhraseCmpdID' + x +'" class="form-control" style="display: none; width: 80%" onchange="saveFilterInputs()"></textarea></div> \
                        </div>');

        $('#searchPhraseCmpdID' + x).append(textInput2(x));
        loadSelectedfields(x);
        getSymbol(x); // this is the default symbols every time a new filter row is added
        if (x === 1){
            $('#entry' + x).append('<img src="images/plusAddRow.png" class="add_field_button" width: 10%; onclick="addFilterRow(' + x + ');"/>');
        }
        else {
            $('#operatorSelect' + x).append(getOperator(x)); // we just add the operator not on the first row
            // $('#entry' + x).append('<img src="images/plusAddRow.png" class="add_field_button" width: 5%; onclick="addFilterRow(' + x + ');"/> &nbsp;');
            $('#entry' + x).append('<img src="images/plusAddRow.png" class="add_field_button" width: 5%; onclick="addFilterRow(' + x + ');"/> ');
            $('#entry' + x).append('<img src="images/minusRemoveRow.png" class="remove_field" width: 5%; onclick="removeFilterRow(' + x + ');"/>');
            setDomIDIndex('select' + x);
        }
        // duplicate the last filter row in savedFilterInputs
        savedFilterInputs = savedFilterInputs.concat(savedFilterInputs[savedFilterInputs.length -1]);
        // loop over the filter rows in savedFilterInputs backwards and copy the row above to the one below until you get the row whwere + was pressed
        for (var j = savedFilterInputs.length-1; j >= i; j--){
            savedFilterInputs[j] = savedFilterInputs[j-1];
            savedFilterInputs[j].filterRow++;
        }

        getSymbol(i); // get symbol for the row where + was pressed
        // put an empty filter row at the position where the + was pressed
        savedFilterInputs[i] = { 'filterRow': i,
                                'operator': "AND",//(i===1) ? "AND": savedFilterInputs[i+1].operator,
                                'selectedField': selected_fields[0],//$("div#tableCols" + i + " select").val(),
                                // 'symbol': updateSymbols(1,selected_fields[0]),
                                'symbol': $("div#symbol" + i + " select").val(),
                                'inputtedText': null,//$("textarea#searchPhraseCmpdID" + i).val(),
                                'inputtedDateA': $('#inputDTpicker'+ i +'a').val(),
                                'inputtedDateB': $('#inputDTpicker'+ i +'b').val()
                            };
        //     savedFilterInputs[i+1].operator = "AND";
    }
    else{
        console.log("i > x: ", i, x)
    }
    reApplyFilterInputs();
    // saveAndRetainFilterInputs();
    console.log("after: ", savedFilterInputs);
}

//function addQueryTxtbox() {
//    $('#queryTxtboxDiv').append('<div><a href="#" onclick="enableQueryBuilderBox()">Edit</a>&nbsp;<a href="#" onclick="clearQueryBuilderBox()">Clear</a> \
//                                <textarea rows="7" id="queryTxtArea" class="form-control" style="width: 70%" disabled \
//                                placeholder="Select your fields of interest above and build your query/filter below to automatically build your expression. The expression can be edited, cleared, or pasted from your clipboard."></textarea> \
//                                </div>');
//}

function addQueryTxtbox() {
    $('#queryTxtboxDiv').append('<div id="querytxtbox"><textarea rows="7" id="queryTxtArea" class="form-control" style="width: 70%" disabled    placeholder="The SQL expression will be exposed or updated here after the View Summary or Search button is pressed.\ The expression can be edited, cleared, or pasted from your clipboard. Please note, once you click on Edit or Clear, all field selections and filters generated above will be cleared."></textarea> <a href="#" onclick="enableQueryBuilderBox()">Edit</a>&nbsp  &nbsp;<a href="#" onclick="clearQueryBuilderBox()">Clear</a> \
    </div>');
}

function setDomIDIndex(domIDfromHTML) {
    domID = '#' + domIDfromHTML;
    var selectedCol = $(domID).val(); // get the selected column name from drop down
    var idIndex = domID.slice(-1); // get the index of the html id
    updateSymbols(idIndex, selectedCol);
}

function enableQueryBuilderBox() {
    isEditQuery = true;
    $('#queryTxtArea').removeAttr('disabled');
    clearSixLayers();
    clearFilters();
    $('#summary_button').addClass('disabled');
}

function clearQueryBuilderBox() {
    $('#queryTxtArea').val('');
    // clearFourLayers();
    clearSixLayers();
    clearFilters();
    $('#summary_button').addClass('disabled');
    $("#download_button").hide();
    $("#save_sql_button").hide();
}

$(document).ready(function() {

    // hide the main page components first
    $('.categoriesMainDiv').hide();
    $('.datamarts-viewsMainDiv').hide();
    $('.datatypesMainDiv').hide();
    $('.fields-resultsMainDiv').hide();
    $('.hr1').hide();
    $('.hr2').hide();
    $('.searchresheader').hide();
    $("#searchModal").modal({
        show: false,
        backdrop: 'static',
        keyboard: false
    });

    // trigger when user clicked on the summary button
    $("#summary_button").click(function(){
        if((selected_category ==='' || selected_datamartView === '' || !selected_fields.length || //if 4 layers is populated
        (($('textarea#searchPhraseCmpdID1').length != 0 && $('textarea#searchPhraseCmpdID1').val() === '') && $('#symbolSelect1').val()!=='exists')) //if filter criteria is populated
        && !isEditQuery ) {
        alert("Please select atleast one field and one filter condition.");
    }else{
        $(this).prop('disabled', true);
        $("#submit_button").prop('disabled', true);
        $("#cancel_button").prop('diabled', false);
        $("#download_button").prop('disabled', true);
        $("#save_sql_button").prop('disabled', true);
        $(this).text('Retrieving Summary...');
        hideResultCount();
        hidePrevButton();
        hidePageNum();
        hideNextButton();
        hideResultsTable();
        getSummaryTable();
    }
    });

    // trigger when user clicked on the search button
    $("#submit_button").click(function(){
    if((selected_category ==='' || selected_datamartView === '' || !selected_fields.length || //if 4 layers is not populated
        (($('textarea#searchPhraseCmpdID1').length != 0 && $('textarea#searchPhraseCmpdID1').val() === '') && $('#symbolSelect1').val()!=='exists')) //if filter criteria is not populated
        && !isEditQuery) {
        alert("Please select atleast one field and one filter condition.");
    }else{
        $(this).prop('disabled', true);
        $("#summary_button").prop('disabled', true);
        $("#cancel_button").prop('disabled', false);
        $("#download_button").prop('disabled', true);
        $("#save_sql_button").prop('disabled', true);
        $(this).text('Searching...');
        hideSummaryTable();
        runSrchQuery();
    }
    });

   // triggered when user hits cancel button during a searching
   $("#cancel_button").click(function(){
    hideResultsLoadingSpinner();
    document.getElementById('disablingDiv').style.display='none';
       console.log("trying to cancel jobReferenceId ===> ", jobReferenceId);
    if(isDownload){
        $('#searchModal').modal('hide');
    }else{
         //call cancel search
        var cancelQueryRequest = gapi.client.bigquery.jobs.cancel({
        'projectId': project_id,
        'jobId': jobReferenceId
        });
        cancelQueryRequest.execute(function(result) {
        console.log("JAKES CODE: After cancel query request");
        console.log(result);
        if(is_summ){
            $("#summary_button").text('View Summary');
             $('.hr2').hide();
            $('.searchresheaderh5').hide();
        }else{
            $("#submit_button").text("Search")
            $("#cancel_button").hide();
            $("#download_button").hide();
            $("#save_sql_button").hide();
            $('.hr2').hide();
            $('.searchresheader').hide();
        }
        enableButtons();
        });
    }

  });

  function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }


    $("#save_sql_button").click(function(){
        var fn = selected_category + ".sql";
        download(fn,queryForCSVdownload);
        $(this).prop('disabled', true);
        $(this).text('SQL Saved!');
    });
    // download button to export the result to csv
    $("#download_button").click(function(){
        $(this).prop('disabled', true);
        $(this).text('Preparing Download...');
        isDownload = true;
        //if query results is more than 10000000 show download warning
        if(resultCount > 10000000) {
            var mymodal = $('#searchModal');
            mymodal.find('.modal-body').text(downloadModalBody);
            mymodal.find('.modal-footer').find('.btn-secondary').html("Cancel Download");
            mymodal.find('.modal-footer').find('.btn-primary').html("Ok");
            mymodal.modal('show');
        }else{
            exportCSV();
        }
    });

    // trigger when user clicked on the show all
    $(".showAll").click(function(){
        console.log('======> Show All Clicked');
        $('.datatypes').find('btn-group').addClass('show');
        $('.datatypes').find('.multiselect-container').addClass('show');
        $('.datatypes').find('.multiselect-container').find('.multiselect-all').find('a.multiselect-all').find('input').click();
        $('.datatypes').find('.multiselect-container').removeClass('show');
        $('.datatypes').find('btn-group').removeClass('show');
    });
   $(".btn-primary").click(function(){
    if(isDownload){
         $('#searchModal').modal('hide');
         exportCSV();
    }else{
        showResultsLoadingSpinner();
        $('#searchModal').modal('hide')
        document.getElementById('disablingDiv').style.display='block';

        //retry query after 3 mins timeout
        retryQuery(jobReferenceId);
    }

   });

   $(".btn-secondary").click(function(){
    if(isDownload){
        $('#searchModal').modal('hide');
    }else{
         //call cancel search
        var cancelQueryRequest = gapi.client.bigquery.jobs.cancel({
        'projectId': project_id,
        'jobId': jobReferenceId
        });
        cancelQueryRequest.execute(function(result) {
        console.log("JOYS CODE: After cancel query request");
        console.log(result);
        if(is_summ){
            $("#summary_button").text('View Summary');
             $('.hr2').hide();
            $('.searchresheaderh5').hide();
        }else{
            $("#submit_button").text("Search")
            $('#searchModal').modal('hide')
            $("#download_button").hide();
            $("#save_sql_button").hide();
            $('.hr2').hide();
            $('.searchresheader').hide();
        }
        enableButtons();
        });
    }

  });



});
