export const environment = {
    production: true,

    region: 'us-east-2',

    identityPoolId: 'us-east-2:7968b13c-7905-4e67-b5f1-e48263580e99',
    userPoolId: 'us-east-2_2AZnfFoGO',
    clientId: '2qiq4b0kk6lq5q9oqfode28qvp',

    rekognitionBucket: 'rekognition-pics',
    albumName: "usercontent",
    bucketRegion: 'us-east-2',

    ddbTableName: 'LoginTrailmerck',

    cognito_idp_endpoint: '',
    cognito_identity_endpoint: '',
    sts_endpoint: '',
    dynamodb_endpoint: '',
    s3_endpoint: ''
};

